//
//  AppDelegate.h
//  GUICrypt
//
//  Created by Yonatan Rubenstein on 6/28/16.
//  Copyright © 2016 Yonatan Rubenstein. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

