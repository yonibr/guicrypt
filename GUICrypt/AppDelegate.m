//
//  AppDelegate.m
//  GUICrypt
//
//  Created by Yonatan Rubenstein on 6/28/16.
//  Copyright © 2016 Yonatan Rubenstein. All rights reserved.
//

#import "AppDelegate.h"
#import "WindowController.h"
@interface AppDelegate ()

@property(strong) WindowController* window;

@end


@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self setWindow:[[WindowController alloc] init]];
    [self.window showWindow:nil];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
