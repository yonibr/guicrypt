//
//  Encryptor.h
//  GUICrypt
//
//  Created by Yonatan Rubenstein on 6/28/16.
//  Copyright © 2016 Yonatan Rubenstein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Encryptor : NSObject

- (NSData *)encryptString:(NSString *)string withKey:(NSString *)key;
- (NSData *)encryptData:(NSData *)data withKey:(NSString *)key;
- (NSData *)decryptData:(NSData *)data withKey:(NSString *)key;
- (NSString *)getStringFromEncryptedData:(NSData *)data withKey:(NSString *)
key;
@end
