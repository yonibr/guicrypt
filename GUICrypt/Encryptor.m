//
//  Encryptor.m
//  GUICrypt
//
//  Created by Yonatan Rubenstein on 6/28/16.
//  Copyright © 2016 Yonatan Rubenstein. All rights reserved.
//

#import "Encryptor.h"
#import <RNCryptor/RNCryptor.h>

@implementation Encryptor

- (NSData *)encryptString:(NSString *)string withKey:(NSString *)key
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [self encryptData:data withKey:key];
}

- (NSData *)encryptData:(NSData *)data withKey:(NSString *)key
{
    NSData *ciphertext = [RNCryptor encryptWithData:data withPassword:key];
    
    return ciphertext;
}

- (NSData *)decryptData:(NSData *)data withKey:(NSString *)key
{
    NSError *error = nil;
    NSData *plaintext = [RNCryptor decryptWithData:data withPassword:key error:&error];
    if (error != nil) {
        NSLog(@"ERROR: %@", error);
        return nil;
    }
    return plaintext;
    
}

- (NSString *)getStringFromEncryptedData:(NSData *)data withKey:(NSString *)key
{
    NSData *result = [self decryptData:data withKey:key];
    return [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
}
@end
