//
//  WindowController.m
//  GUICrypt
//
//  Created by Yonatan Rubenstein on 6/28/16.
//  Copyright © 2016 Yonatan Rubenstein. All rights reserved.
//

#import "WindowController.h"
#import "Encryptor.h"

#include <sys/xattr.h>


#define ENCRYPTION_TARGET_TEXT @"Encrypt text"
#define ENCRYPTION_TARGET_FILE @"Encrypt file"

#define FILE_EXTENDED_ATTRIBUTE_NAME "com.yr.GUICrypt.encrypted"

@interface WindowController ()
@property (strong) IBOutlet NSTextField *sizeOfTextLabel;
@property (strong) IBOutlet NSTextField *sizeOfFileLabel;
@property (strong) IBOutlet NSTextField *urlLabel;
@property (strong) IBOutlet NSTextField *fileLoadedLabel;
@property (strong) IBOutlet NSTextField *dataInRAMLabel;
@property (strong) IBOutlet NSTextField *dataInRamIsEncryptedLabel;

@property (strong) IBOutlet NSButton *encryptInPlaceToggleButton;
@property (strong) IBOutlet NSSecureTextField *encryptionKeyField;
@property (strong) IBOutlet NSSecureTextField *encryptionTextField;
@property (strong) IBOutlet NSTextField *decryptedTextField;
@property (strong, nonatomic) NSData *encryptedTextData;
@property (strong, nonatomic) NSData *fileData;
@property (strong, nonatomic) NSString *encryptionTargetType;
@property (strong, nonatomic, setter=setFileURL:) NSURL *fileURL;

typedef NS_ENUM(NSInteger, EncryptionStatus) {
    ENCRYPTION_STATUS_UNKNOWN,
    ENCRYPTION_STATUS_ENCRYPTED,
    ENCRYPTION_STATUS_NOT_ENCRYPTED
};
@end

@implementation WindowController


#pragma mark - Setup

- (id)init
{
    self = [super initWithWindowNibName:@"WindowController"];
    return self;
}

- (void)windowDidLoad {
    [super windowDidLoad];
    self.encryptionTargetType = ENCRYPTION_TARGET_TEXT;
}

#pragma mark - Encryption and Decryption methods

- (void)encryptText
{
    NSString *toEncrypt = [self.encryptionTextField stringValue];
    NSString* key = [self.encryptionKeyField stringValue];
    
    if ([toEncrypt length])
    {
        self.encryptedTextData = [[[Encryptor alloc] init] encryptString:toEncrypt withKey:key];
        [self setDataInRAMLabelText];
        [self.sizeOfTextLabel setStringValue:[NSByteCountFormatter stringFromByteCount:self.encryptedTextData.length countStyle:NSByteCountFormatterCountStyleFile]];
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Data successfully encrypted in memory."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        

    }
    else
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Please supply text to encrypt."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        
    }
}

- (void)decryptText
{
    NSString *key = [self getKey];
    
    if (self.encryptedTextData && key)
    {
        NSString *decryptedText = [[[Encryptor alloc] init] getStringFromEncryptedData:self.encryptedTextData withKey:key];
        if (!decryptedText)
        {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Incorrect key entered."];
            [alert addButtonWithTitle:@"Ok"];
            [alert runModal];
        }
        else
        {
            [self.decryptedTextField setStringValue:decryptedText];
        }
    }
    else if (key)
    {
        [self.decryptedTextField setStringValue:@"Nothing is currently encrypted."];
    }
}

- (void)encryptFile
{
    if (![self checkForFileData])
    {
        return;
    }
    
    
    self.fileData = [[[Encryptor alloc] init] encryptData:self.fileData withKey:[self.encryptionKeyField stringValue]];
    [self.dataInRamIsEncryptedLabel setStringValue:@"Yes"];
    [self.sizeOfFileLabel setStringValue:[NSByteCountFormatter stringFromByteCount:self.fileData.length countStyle:NSByteCountFormatterCountStyleFile]];

    
    if (self.encryptInPlaceToggleButton.state == NSOnState)
    {
        [self saveData:self.fileData ToURL:self.fileURL];
    }
}

- (void)decryptFile
{
    if (![self checkForFileData])
        return;
    
    NSString *key = [self getKey];
    
    NSData *result = [[[Encryptor alloc] init] decryptData:self.fileData withKey:key];
    
    if (result)
    {
        self.fileData = result;
        [self.sizeOfFileLabel setStringValue:[NSByteCountFormatter stringFromByteCount:result.length countStyle:NSByteCountFormatterCountStyleFile]];

        if (self.encryptInPlaceToggleButton.state == NSOnState)
        {
            [self saveData:self.fileData ToURL:self.fileURL];
        }
        [self.dataInRamIsEncryptedLabel setStringValue:@"No"];
    }
    else
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Decryption failed. Either the file was not encrypted or the key was incorrect."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
    }
    
}


#pragma mark - I/O methods

- (void)openFile
{
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    [panel setCanChooseFiles:YES];
    [panel setCanChooseDirectories:NO];
    [panel setAllowsMultipleSelection:NO];
    
    NSInteger clicked = [panel runModal];
    if (clicked == NSFileHandlingPanelOKButton) {
        self.fileURL = [panel URLs][0];
        self.fileData = [[NSFileManager defaultManager] contentsAtPath:[self.fileURL path]];
        [self.fileLoadedLabel setStringValue:@"Yes"];
        [self setDataInRAMLabelText];
        [self setDataInRamIsEncryptedLabelText];
        [self.sizeOfFileLabel setStringValue:[NSByteCountFormatter stringFromByteCount:self.fileData.length countStyle:NSByteCountFormatterCountStyleFile]];
    }
}

- (void)saveFile
{
    NSSavePanel* panel = [NSSavePanel savePanel];
    [panel setNameFieldStringValue:@"file"];
    NSInteger clicked = [panel runModal];
    
    if (clicked == NSFileHandlingPanelOKButton)
    {
        if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_TEXT])
        {
            [self saveData:self.encryptedTextData ToURL:[panel URL]];
        }
        else if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_FILE])
        {
            [self saveData:self.fileData ToURL:[panel URL]];
        }
    }
}


- (void)setEncryptedProperty:(BOOL)val forFileAtURL:(NSURL *)URL
{
    const char *filePath = [URL fileSystemRepresentation];
    const char *name = FILE_EXTENDED_ATTRIBUTE_NAME;
    const char *value = val ? "1" : "0";
    int result = setxattr(filePath, name, value, strlen(value), 0, 0);
    if (result == -1)
    {
        NSLog(@"Failed to change file extended attribute.");
    }
}

- (void)saveData:(NSData *)data ToURL:(NSURL *)url
{
    if (!data)
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"No data to save."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        return;
    }
    if (!url)
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"No save destination selected."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [data writeToURL:url atomically:YES];
        [self setEncryptedProperty:[self.dataInRamIsEncryptedLabel.stringValue isEqualToString:@"Yes"] forFileAtURL:url];
    });
}


- (EncryptionStatus)getEncryptionStatus
{
    const char *filePath = [[self.fileURL path] cStringUsingEncoding:NSUTF8StringEncoding];
    const char *name = FILE_EXTENDED_ATTRIBUTE_NAME;
    long bufferLength = getxattr(filePath, name, NULL, 0, 0, 0);
    
    if (bufferLength == -1)
        return ENCRYPTION_STATUS_UNKNOWN;
    
    // make a buffer of sufficient length
    char *buffer = (char *)malloc(bufferLength);
    getxattr(filePath, name, buffer, bufferLength, 0, 0);
    
    return buffer[0] == '1' ? ENCRYPTION_STATUS_ENCRYPTED : ENCRYPTION_STATUS_NOT_ENCRYPTED;
}



#pragma mark - Helper methods

- (BOOL)verifyKey
{
    NSString* key = [self.encryptionKeyField stringValue];
    if (![key length])
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Please enter an encryption key before attemting to encrypt anything."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        return NO;
    }
    
    NSString *retypedKey;
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Please verify key:"];
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Cancel"];
    
    NSSecureTextField *input = [[NSSecureTextField alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    [input setStringValue:@""];
    
    [alert setAccessoryView:input];
    NSInteger button = [alert runModal];
    if (button == NSAlertFirstButtonReturn) {
        retypedKey = [input stringValue];
    }
    else if (button == NSAlertSecondButtonReturn) {
        return NO;
    }
    
    return [retypedKey isEqualToString:key] ? YES : NO;
}

- (NSString *)getKey
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Please enter decryption key:"];
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Cancel"];
    
    NSSecureTextField *input = [[NSSecureTextField alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    [input setStringValue:@""];
    
    [alert setAccessoryView:input];
    NSInteger button = [alert runModal];
    if (button == NSAlertFirstButtonReturn) {
        return [input stringValue];
    }
    else {
        return nil;
    }
}

- (BOOL)checkForFileData
{
    if (!self.fileData)
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"No file data in memory."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        return NO;
    }
    return YES;
}

- (void)setDataInRAMLabelText
{
    NSString *labelValue = @"";
    if (self.encryptedTextData)
    {
        labelValue = [labelValue stringByAppendingString:@"Encrypted text    "];
    }
    if (self.fileData)
    {
        labelValue = [labelValue stringByAppendingString:@"File"];
    }
    [self.dataInRAMLabel setStringValue:labelValue];
}

- (void)setDataInRamIsEncryptedLabelText
{
    EncryptionStatus status = [self getEncryptionStatus];
    if (status == ENCRYPTION_STATUS_NOT_ENCRYPTED)
    {
        [self.dataInRamIsEncryptedLabel setStringValue:@"No"];
    }
    else if (status == ENCRYPTION_STATUS_ENCRYPTED)
    {
        [self.dataInRamIsEncryptedLabel setStringValue:@"Yes"];
    }
    else
    {
        [self.dataInRamIsEncryptedLabel setStringValue:@"Unknown"];
    }
}



#pragma mark - IBActions

- (IBAction)onDecryptButtonPressed:(NSButton *)sender {
    if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_TEXT])
    {
        [self decryptText];
    }
    if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_FILE])
    {
        [self decryptFile];
    }
    
}

- (IBAction)onEncryptButtonPressed:(NSButton *)sender {

    if (![self verifyKey])
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Failed to verify key. No encryption was performed."];
        [alert addButtonWithTitle:@"Ok"];
        [alert runModal];
        return;
    }
    
    if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_TEXT])
    {
        [self encryptText];
    }
    else if ([self.encryptionTargetType isEqualToString:ENCRYPTION_TARGET_FILE])
    {
        [self encryptFile];
    }
}

- (IBAction)onRadioButtonToggled:(NSButton *)sender {
    if ([sender.title isEqualToString:ENCRYPTION_TARGET_TEXT])
    {
        self.encryptionTargetType = ENCRYPTION_TARGET_TEXT;
    }
    else if ([sender.title isEqualToString:ENCRYPTION_TARGET_FILE])
    {
        self.encryptionTargetType = ENCRYPTION_TARGET_FILE;
    }
}

- (IBAction)onChooseFileButtonPressed:(NSButton *)sender {
    [self openFile];
}

- (IBAction)onSaveFileButtonPressed:(NSButton *)sender {
    [self saveFile];
}

- (IBAction)onClearRAMButtonPressed:(NSButton *)sender {
    self.fileData = nil;
    self.encryptedTextData = nil;
    [self setDataInRAMLabelText];
    [self.sizeOfTextLabel setStringValue:[NSByteCountFormatter stringFromByteCount:0 countStyle:NSByteCountFormatterCountStyleFile]];
    [self.sizeOfFileLabel setStringValue:[NSByteCountFormatter stringFromByteCount:0 countStyle:NSByteCountFormatterCountStyleFile]];
}

#pragma mark - Getter and setter methods

- (void)setFileURL:(NSURL *)fileURL
{
    _fileURL = fileURL;
    [self.urlLabel setStringValue:[fileURL path]];

}


@end
