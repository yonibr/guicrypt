# GUICrypt

GUICrypt is a macOS encryption program. It provides a GUI interface which can:

  - Perform AES Encryption on:
    -  Text
    -  Any file
        - In place
        - Anywhere you want
  - Decrypt files it previously encrypted (provided you have the key used for encryption)
  - Remeber if it encrypted or decrypted a file already


AES is an encryption standard used by many orginizations and people, including government agencies.  According to the [AES] website, 

> AES (acronym of Advanced Encryption Standard) is a symmetric encryption algorithm.
> The algorithm was developed by two Belgian cryptographer Joan Daemen and Vincent Rijmen.
> AES was designed to be efficient in both hardware and software, and supports a block length of 128 bits and key lengths of 128, 192, and 256 bits.

### Version
0.1

### Tech

GUICrypt uses the following open source projects:

* [RNCryptor] - Open source wrapper for iOS and macOS written in Swift. 

### Installation

Only works on macOS. Currently written and tested on macOS 10.12.

Runs through XCode. A binary is available [here].

### Development

Any and all contributions are welcome, including coding support, feature suggestions, and bug reports.


### Todos

 - Either disable ability to encrypt same file multiple times or add xattr that stores number of times a file has been encrypted
 - File preview
 - Drag and drop from finder
 - Add an icon
 - Add a help screen
 - Add compression
 - Other TBD features

Licenses
----
**GUICrypt**

MIT

>Copyright (c) 2016 Yoni Rubenstein

>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**RNCryptor**
>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. ```



Readme created with [Dillinger].


   [AES]: <http://aesencryption.net>
   [RNCryptor]: <https://github.com/RNCryptor/RNCryptor>
   [Dillinger]: <http://dillinger.io>
   [here]: <https://bitbucket.org/yonibr/guicrypt/downloads>